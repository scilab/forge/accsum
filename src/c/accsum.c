// Copyright (C) 2010-2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include "accsum.h"

#include <math.h>

// References
//
// "Stability and numerical accuracy of algorithms", Nicolas Higham
//
// "Handbook of Floating Point Computations", Muller et al
//
// https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img14.htm
// https://hpcrd.lbl.gov/SCG/ocean/NRS/SCSsum.F
//
// "Using Accurate Arithmetics to Improve Numerical Reproducibility "
// and Stability in Parallel Applications".
// Yun He and Chris H.Q. Ding.  Journal of Supercomputing,
// Vol.18, Issue 3, 259-277, March 2001.
// Also Proceedings of International Conference on Supercomputing (ICS'00),
// May 2000, 225-234.

  // Returns s and e such that s+e is the sum of x.
  //   s : the highest significant digits of the sum
  //   e : the lowest significant digits of the sum
  // A Doubly Self Compensated Sum algorithm.
  // Uses accsum_fasttwosum.
  // Bibliography
  // "Stability and numerical accuracy of algorithms", Nicolas Higham
  // "Handbook of Floating Point Computations", Muller et al
  // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img14.htm
  // https://hpcrd.lbl.gov/SCG/ocean/NRS/SCSsum.F
void accsum_fdcs(double * x, int n, double * s, double * e)
{
    double s1;
    double e1;
    double s2;
    double e2;
    int i;

    *s = 0;
    *e = 0;
    for (i = 0; i < n; i++)
    {
        accsum_fasttwosum(*e, x[i], &s1, &e1);
        accsum_fasttwosum(*s, s1, &s2, &e2);
        accsum_fasttwosum(s2, e1 + e2, s, e);
    }
}

  // Returns s and e such that s+e is the sum of x.
  //   s : the highest significant digits of the sum
  //   e : the lowest significant digits of the sum
  // A Self Compensated Sum algorithm.
  // Uses accsum_fasttwosum.
  // Bibliography
  // "Stability and numerical accuracy of algorithms", Nicolas Higham
  // "Handbook of Floating Point Computations", Muller et al
  // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img14.htm
  // https://hpcrd.lbl.gov/SCG/ocean/NRS/SCSsum.F
void accsum_fscs(double * x, int n, double * s, double * e)
{
    double s1;
    double e1;
    int i;

    *s = 0;
    *e = 0;
    for (i = 0; i < n; i++)
    {
        accsum_fasttwosum(*s, x[i] + *e, &s1, &e1);
        *s = s1;
        *e = e1;
    }
}

  // Returns s and t so that a + b = s + t exactly.
  // where s contains the highest significant digits
  // and t the lowest significant digits.
  //
  // Assumes that |a|>=|b|. If not, the arguments are switched.
  //
  // Algorithm 4.3 in HFCC
  // Due to Dekker (and Kahan)
  // Assumes that we use round-to-nearest.
void accsum_fasttwosum(double a, double b, double * s, double * t)
{
    double tmp;
    double z;

    if (fabs(a) < fabs(b))
    {
        // Switch a and b
        tmp = a;
        a = b;
        b = tmp;
    }
    *s = a + b;
    z = *s - a;
    *t = b - z;
}

void accsum_twosum(double a, double b, double * s, double * t)
{
    // The twosum sum of a and b.
    // Description
    // Returns the sum of a and b
    // so that a + b = s + t exactly.
    // where s contains the highest significant digits
    // and t the lowest significant digits.
    // Algorithm 4.4 in HFCC
    // Due to Knuth
    // No assumption on a , b.
    // Assumes that we use round-to-nearest.
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    double ap;
    double bp;
    double da;
    double db;

    *s = a + b;
    ap = *s - b;
    bp = *s - ap;
    da = a - ap;
    db = b - bp;
    *t = da + db;
}

void accsum_fcompsum(double * x, int n, double * s, double * e)
{
    // The compensated sum of a matrix.
    // Description
    // Returns s and e such that s+e is the sum of x.
    //   s : the highest significant digits of the sum
    //   e : the lowest significant digits of the sum
    // Algorithm 4.4 in HFCC
    // Due to Knuth
    // No assumption on a , b
    // Assumes that Scilab uses round-to-nearest.
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    int i;
    double t;
    double y;

    *s = 0;
    *e = 0;
    for (i = 0; i < n; i++)
    {
        t = *s;
        y = x[i] + *e;
        *s = t + y;
        *e = (t - *s) + y;
    }
}

