// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#ifndef _ACCSUM_H_
#define _ACCSUM_H_

#ifdef _MSC_VER
    #if LIBACCSUM_EXPORTS
        #define ACCSUM_IMPORTEXPORT __declspec (dllexport)
    #else
        #define ACCSUM_IMPORTEXPORT __declspec (dllimport)
    #endif
#else
    #define ACCSUM_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

// Sets s as the sum of x entries, with a double compensated summation.
void accsum_fdcs(double * x, int n, double * s, double * e);

// Sets s+e as the sum of a and b
void accsum_fasttwosum(double a, double b, double * s, double * e);

// Sets s as the sum of x entries, with a simply compensated summation.
void accsum_fscs(double * x, int n, double * s, double * e);

// The compensated sum of a matrix.
void accsum_fcompsum(double * x, int n, double * s, double * e);

__END_DECLS

#endif /* _ACCSUM_H_ */

