// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s, t] = accsum_fasttwosum(a, b)
    // The fast2sum sum of a and b.
    //
    // Calling Sequence
    //   s = accsum_fasttwosum ( a , b )
    //   [s,t] = accsum_fasttwosum ( a , b )
    //
    // Parameters
    //   a : a 1-by-1 matrix of doubles
    //   b : a 1-by-1 matrix of doubles
    //   s : a 1-by-1 matrix of doubles, the highest significant
    //       digits of the sum
    //   t : a 1-by-1, matrix of doubles, the lowest significant
    //       digits of the sum
    //
    // Description
    // Returns the sum of a and b
    // so that a + b = s + t exactly.
    // where s contains the highest significant digits
    // and t the lowest significant digits.
    //
    // Enforces |a| >= |b|
    //
    // Algorithm 4.3 in HFCC
    // Due to Dekker (and Kahan)
    // Assumes that Scilab uses round-to-nearest.
    //
    // Examples
    //  [s,t] = accsum_fasttwosum ( 2 , 1 ) // 3
    // // is 1+(%eps/2) but is 1 without algo
    //  [s,t] = accsum_fasttwosum ( 1 , %eps/2 )
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_fasttwosum", rhs, 2:2);
    apifun_checklhs("accsum_fasttwosum", lhs, 0:2);

    if abs(a) < abs(b) then
        // Switch a and b
        tmp = a;
        a = b;
        b = tmp;
    end

    s = a + b;
    z = s - a;
    t = b - z;
endfunction

