// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = accsum_priestx()
    // A difficult example for SCS by Priest.
    //
    // Calling Sequence
    //   x = accsum_priestx()
    //
    // Parameters
    //   x : a 6-by-1, matrix of doubles
    //
    // Description
    // "On properties of floating point arithmetics:
    // numerical stability and the cost of accurate computations"
    // Douglas Priest, 1992
    // p62
    // "What is the smallest n for which simply compensated
    // summation is not guaranteed to yield a result
    // with a small forward relative error in an arithmetic which
    // conforms to the IEEE 754 standard. The smallest known
    // to the author is ... Then the exact sum is 2, but the sum
    // computed by the SCS is 3."
    //
    // Examples
    // x = accsum_priestx()
    // [s, e] = accsum_scs(x) // = 3 + 0
    // [s, e] = accsum_dcs(x) // = 2 + 0

    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al
    // "On properties of floating point arithmetics: numerical stability
    // and the cost of accurate computations", Douglas Priest, 1992

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_priestx", rhs, 0:0);
    apifun_checklhs("accsum_priestx", lhs, 0:1);

    t = 53;
    x(1) = 2 ^ (t+1);
    x(2) = 2 ^ (t+1) - 2;
    x(3:6) = - (2 ^ t - 1);
endfunction

