// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s, e] = accsum_dblcompsum(x)
    // The doubly compensated sum of a matrix.
    //
    // Calling Sequence
    //   s = accsum_dblcompsum ( x )
    //   [s,e] = accsum_dblcompsum ( x )
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   s : a 1-by-1 matrix of doubles, the highest significant
    //       digits of the sum
    //   e : a 1-by-1, matrix of doubles, the lowest significant
    //       digits of the sum
    //
    // Description
    // Returns the sum of x.
    // Algorithm 4.2 in SNAA (Doubly Compensated Summation)
    // Due to Priest (and Kahan)
    // Assumes that Scilab uses round-to-nearest.
    //
    // Examples
    // [s,e] = accsum_dblcompsum ( [2 1] ) // 3
    // [s,e] = accsum_dblcompsum ( [1 2] ) // 3
    // // is 1+(%eps/2) but is 1 without algo
    // [s,e] = accsum_dblcompsum ( [1 %eps/2 ] )
    // // is 1+(%eps/2) but is 1 without algo
    // [s,e] = accsum_dblcompsum ( [%eps/2 1] )
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_dblcompsum", rhs, 1:1);
    apifun_checklhs("accsum_dblcompsum", lhs, 0:2);

    // Sort x in decreasing magnitude order.
    [B, k] = gsort(abs(x));
    x = x(k);
    // Perform the sum
    s = x(1);
    e = 0;
    n = size(x, "*");
    for i = 2 : n
        y = e + x(i);
        u = x(i) - (y - e);
        t = y + s;
        v = y - (t - s);
        z = u + v;
        s = t + z;
        e = z - (s - t);
    end
endfunction

