// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function s = accsum_orderdynamic(x, order)
    // Compute the sum with a dynamic re-ordering.
    //
    // Calling Sequence
    //   s = accsum_orderdynamic ( x , order )
    //
    // Parameters
    //   x : a m-by-n, matrix of doubles
    //   order : a 1-by-1 matrix of doubles, a positive integer, the order.
    //           order = 1 : search for the entry which makes the smallest
    //                       |s+y(i)|, good for accuracy,
    //           order = 2 : search for the entry which makes the largest
    //                       |s+y(i)|, bad for accuracy
    //   s : a 1-by-1 matrix of doubles, the sum
    //
    // Description
    // Try to find an order which makes the sum perform bad/good.
    // Finding the optimal ordering of a sum, which minimizes the
    // intermediate sums, is a combinatorial problem which is too
    // expensive to solve in general.
    // A compromise is used here.
    // For k from 1 to n,
    // we search for i which minimizes (if order=1) or maximizes
    // (if order=2) the term |s+x(i)|, among the entries from
    // 1 to n-k+1.
    // Then the entries #i and #n-k+1 are switched.
    //
    // This algorithm is called "Psum" in the Chapter 4 "Summation" of
    // SNAA.
    //
    // Examples
    // // Test on simple data
    // x = [-9.9 5.5 2.2 -3.3 -6.6 0 1.1]
    // sum(x)
    // s = accsum_orderdynamic ( x , 1 )
    // s = accsum_orderdynamic ( x , 2 )
    //
    // // Test on difficult data
    // path = accsum_getpath (  );
    // filename=fullfile(path,"demos","etaana.dat");
    // x=fscanfMat(filename);
    // // 7.390949249267578125000
    // sum(x)
    // // 0.3437500000000000000000
    // s = accsum_orderdynamic ( x , 1 )
    // // - 73.015625
    // s = accsum_orderdynamic ( x , 2 )
    //
    // Authors
    // Michael Baudin, 2010-2011
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al
    // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img16.htm

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_orderdynamic", rhs, 2:2);
    apifun_checklhs("accsum_orderdynamic", lhs, 0:1);

    s = 0;
    z = x;
    n = size(x, "*");
    // We remove an entry of z at each iteration of the loop:
    // in the end, the array z is empty.
    for k = 1 : n
        // Sort into increasing order, from 1 to n-k+1.
        [B, i] = gsort(abs(s+z(1:n-k+1)), "g", "i");
        if order == 1 then
            // Set in i the indice which makes |s+z(i)| minimum
            i = i(1);
        else
            // Set in i the indice which makes |s+z(i)| maximum
            i = i($);
        end
        // Take into account for entry #i
        s = s + z(i);
        // Switch entries #n-k+1 and #i
        t = z(i);
        z(i) = z(n-k+1);
        z(n-k+1) = t;
    end
endfunction

