// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = accsum_wilkinson(r)
    // A test vector by Wilkinson.
    //
    // Calling Sequence
    //   x = accsum_wilkinson(r)
    //
    // Parameters
    //   r : a 1-by-1 matrix of doubles, a positive integer
    //   x : a 1-by-r, matrix of doubles
    //
    // Description
    // Returns a vector x of size 2^r where r is a flint.
    // Exercise 4.2 in SNAA
    //
    // Examples
    // // Example with r = 3
    // x = accsum_wilkinson (3)
    // // Expected result
    // e = [1
    //      1 -   %eps
    //      1 - 2*%eps
    //      1 - 2*%eps
    //      1 - 4*%eps
    //      1 - 4*%eps
    //      1 - 4*%eps
    //      1 - 4*%eps];
    //
    // // Use with larger r
    // x = accsum_wilkinson(10);
    // s = sum(x);
    // [s, e] = accsum_compsum(x);
    // [s, e] = accsum_dblcompsum(x);
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_wilkinson", rhs, 1:1);
    apifun_checklhs("accsum_wilkinson", lhs, 0:1);

    n = 2^r;
    t = 53;
    u = 2^(-t); // would give the same as u = %eps
    x(1) = 1;
    x(2) = 1 - u;
    for k = 2 : r
        x(2^(k-1) + 1 : 2^k) = 1 - 2^(k-1) * u;
    end
endfunction

