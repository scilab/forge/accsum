// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = accsum_order(x, o)
    // Re-order the matrix.
    //
    // Calling Sequence
    //   y = accsum_order(x, o)
    //
    // Parameters
    //   x : a m-by-n, matrix of doubles
    //   o : a 1-by-1 matrix of doubles, a positive integer, the order.
    //       o = 1 : no-op i.e. y = x,
    //       o = 2 : increasing order,
    //       o = 3 : decreasing order,
    //       o = 4 : increasing magnitude order,
    //       o = 5 : decreasing magnitude order (good order for DCS),
    //       o = 6 : positive reverse from order 2,
    //       o = 7 : negative reverse from order 2,
    //       o = 8 : random order
    //   y : a m-by-n, matrix of doubles, the same content as x,
    //       but in different order.
    //
    // Description
    // Re-order the matrix.
    // If o=8, then this function uses the grand function and
    // modifies its state.
    //
    // The increasing magnitude order (o=4) is good when summing nonnegative
    // numbers by recursive summation (i.e. with accsum_straight),
    // in the sense of having the smallest a priori forward error bound.
    //
    // The decreasing magnitude ordering order (o=5) is good when
    // there is heavy cancellation, i.e. when the condition number
    // returned by accsum_sumcond is large.
    // In this case, the recursive summation (i.e. accsum_straight)
    // can be used with decreasing magnitude ordering.
    //
    // Examples
    // x = [-9.9 5.5 2.2 -3.3 -6.6 0 1.1]
    // for o = 1 : 8
    //   y = accsum_order(x, o);
    //   disp([o y'])
    // end
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al
    // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img16.htm

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_order", rhs, 2:2);
    apifun_checklhs("accsum_order", lhs, 0:1);

    x = x(:);
    n = size(x, "*");
    select o
      case 1
        k = 1:n;
      case 2
        [B, k] = gsort(x, "g", "i");
      case 3
        [B, k] = gsort(x, "g", "d");
      case 4
        [B, k] = gsort(abs(x), "g", "i");
      case 5
        [B, k] = gsort(abs(x), "g", "d");
      case 6
        k1 = find(x<=0);
        [B1, k11] = gsort(x(k1), "g", "i");
        k2 = find(x>0);
        [B, k22] = gsort(x(k2), "g", "d");
        k = [k1(k11) k2(k22)];
      case 7
        k1 = find(x<0);
        [B, k11] = gsort(x(k1), "g", "d");
        k2 = find(x>=0);
        [B, k22] = gsort(x(k2), "g", "i");
        k = [k1(k11) k2(k22)];
      case 8
        k = grand(1, "prm", (1:n)');
    else
        error(msprintf("Unkown order %d", o));
    end
    y = x(k);
endfunction

