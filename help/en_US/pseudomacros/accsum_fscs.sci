// Copyright (C) 2010-2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s,e] = accsum_fscs ( x )
  // A Self Compensated Sum algorithm
  // 
  // Calling Sequence
  //   s = accsum_fscs ( x )
  //   [s,e] = accsum_fscs ( x )
  // 
  // Parameters
  //   x : a m-by-n, matrix of doubles
  //   s : a 1-by-1 matrix of doubles, the highest significant digits of the sum
  //   e : a 1-by-1, matrix of doubles, the lowest significant digits of the sum
  // 
  // Description
  // A Self Compensated Sum algorithm.
  // Uses the fasttwosum algorithm.
  // This is a fast implementation, based on compiled source code.
  // 
  // Examples
  // s = accsum_fscs ( [2 1] ) // 3
  // s = accsum_fscs ( [1 2] ) // 3
  // x = accsum_wilkinson(10); size(x,"*")
  // s = sum(x)
  // s = accsum_fscs ( x )
  //
  // Authors
  // Michael Baudin, 2010-2011
  //
  // Bibliography
  // "Stability and numerical accuracy of algorithms", Nicolas Higham
  // "Handbook of Floating Point Computations", Muller et al
  // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img14.htm
  // https://hpcrd.lbl.gov/SCG/ocean/NRS/SCSsum.F

endfunction

