// Copyright (C) 2010-2011 - Michael Baudin

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating accsum\n");
helpdir = cwd;
funmat = [
  "accsum_compsum"
  "accsum_dblcompsum"
  "accsum_dcs"
  "accsum_fasttwosum"
  "accsum_orderdynamic"
  "accsum_scs"
  "accsum_straight"
  "accsum_twosum"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "accsum";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the library help
mprintf("Updating accsum/support\n");
helpdir = fullfile(cwd,"support");
funmat = [
  "accsum_higham"
  "accsum_order"
  "accsum_priestx"
  "accsum_shuffle"
  "accsum_sumcond"
  "accsum_wilkinson"
  "accsum_getpath"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "accsum";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


//
// Generate the library help
mprintf("Updating accsum/pseudomacros\n");
helpdir = cwd;
funmat = [
  "accsum_fdcs"
  "accsum_fscs"
  "accsum_fcompsum"
  ];
macrosdir = fullfile(cwd,"pseudomacros");
demosdir = [];
modulename = "accsum";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
