// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// A comment from the paper:
// "Among the 6 different orders, the one with decreasing
// magnitude order produces the correct answer. Intuitively in this 
// order, the numbers with similar magnitude bug opposite signes are added
// together first, and then the result is added to the accumulation 
// without much loss of precision. This order is the recommended 
// order in summation of large arrays."

// Test this on sea surface height data :
// https://hpcrd.lbl.gov/SCG/ocean/NRS/
// "Using Accurate Arithmetics to Improve Numerical Reproducibility and Stability in 
// Parallel Applications". Yun He and Chris H.Q. Ding.  Journal of Supercomputing,
// Vol.18, Issue 3, 259-277, March 2001. Also Proceedings of International Conference
// on Supercomputing (ICS'00), May 2000, 225-234.
//
// Thanks to A. Edelman for pointing this paper.

path = accsum_getpath (  );
filename=fullfile(path,"demos","etaana.dat");
x=fscanfMat(filename);
size(x)
// Display the spread of the data
// - 1.074D+14    0.0009623    1.434D+13    1.080D+14 
// This is a massive spread !
// The values are large, of opposite signs and are so that the 
// sum is small. This is a worst case for the sum, as it 
// maximizes the possibilities of cancellation.
[min(x) mean(x) stdev(x) max(x)]
s=sum(x)
[s,e] = accsum_compsum ( x ) 
[s,e] = accsum_dblcompsum ( x )
[s,e] = accsum_scs ( x )
[s,e] = accsum_dcs ( x )
// sum(x)                   = 7.390625
// [s,e] = accsum_compsum ( x )    = 0.3823695 + 0.  
// [s,e] = accsum_dblcompsum ( x ) = 0.3579858 + 0.
// [s,e] = accsum_scs ( x )        = 0.3823695 + 0.
// [s,e] = accsum_dcs ( x )        = 0.3882289 + 0.

// Reproduce experiment at : https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img17.htm
// Sensitivity regarding to summation order
mprintf("%-5s %-20s %-20s %-20s %-20s\n","Order","Straight","sum","SCS","DCS");
path = accsum_getpath (  );
filename=fullfile(path,"demos","etaana.dat");
for o = 1 : 8
  x=fscanfMat(filename);
  x = accsum_order ( x , o );
  s0=accsum_straight(x);
  s1=sum(x);
  [s2,e] = accsum_scs ( x );
  [s3,e] = accsum_dcs ( x );
  mprintf("#%-5d %+20.17f %+20.17f %+20.17f %+20.17f\n",o,s0,s1,s2,s3)
end
// Exact result is : +0.35798583924770355
// Order Straight               sum                  SCS                  DCS                 
//#1     +34.41476821899414100  +7.39094924926757810 +0.38236951828002930 +0.38822889328002930
//#2     -70.64062500000000000  -0.64062500000000000 +0.35937500000000000 +0.35937500000000000
//#3     -73.01562500000000000  -0.39062500000000000 +0.35937500000000000 +0.35937500000000000
//#4     +13.85937500000000000  +1.85937500000000000 +0.35937500000000000 +0.35937500000000000
//#5     +14.31865964829921700  +1.31250000000000000 +0.35798583924770355 +0.35798583924770355
//#6     -36.25424389541149100  -2.24288563430309300 +0.35812444984912872 +0.35812444984912872
//#7     -66.64062500000000000 +22.10937500000000000 +0.35937500000000000 +0.35937500000000000
//#8      -0.09179687500000000  +0.30664062500000000 +0.39257812500000000 +0.36132812500000000
// The order #5, combined with simply (or double) compensated summation algo gives the
// correct answer.
// With double-double implementation, the result is: 0.3579858392477035522460937500000


// Conclusions:
// * Catastrophic cancellation may appear, for example when the spread of the data is extreme.
// * The compensated sum is efficient for stabilizing results.
// * When the values are all positive, ordering the values by increasing 
//   magnitude would give the best result.
// * Ordering the data by decreasing magnitude helps to further reduce the error,
//   when the values have mixed signs.
// * The sum function is sensitive to the use of the compiler optimizations, 
//   the Intel MKL or ATLAS libraries, which changes the orders of the computation.
//   In Scilab, we use a straightforward sum function :
//     http://gitweb.scilab.org/?p=scilab.git;a=blob;f=scilab/modules/elementary_functions/src/fortran/dsum.f;h=3e216367a176911f46a16a771ead8cfb8620f74c;hb=HEAD
//   The reason of the difference between the Scilab macro "Straight" and the sum 
//   function is the use of compiler optimizations.
//   Furthermore, the processor may use intermediate extended precision.

//////////////////////////////////////////////////////


// Make a Monte-Carlo simulation for the sum
lines(0);
kmax = 100;
path = accsum_getpath (  );
filename=fullfile(path,"demos","etaana.dat");
x=fscanfMat(filename);
for k = 1 : kmax
  if ( modulo(k,10) == 0 ) then
    disp([k min(sumk) mean(sumk) max(sumk)]);
  end
  x = accsum_order ( x , 8 );
  s=accsum_straight(x);
  sumk(k) = s;
end
disp([min(sumk) mean(sumk) max(sumk)])




//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "ademo.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );

