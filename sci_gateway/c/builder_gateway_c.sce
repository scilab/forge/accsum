// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function builder_gw_c()

    gateway_path = get_absolute_file_path("builder_gateway_c.sce");
    libname = "accsumgateway";
    namelist = [
        "accsum_fdcs"      "sci_accsum_fdcs"
        "accsum_fscs"      "sci_accsum_fscs"
        "accsum_fcompsum"  "sci_accsum_fcompsum"
        ];

    files = [
        "sci_accsum_fdcs.c"
        "sci_accsum_fscs.c"
        "sci_accsum_fcompsum.c"
        ];


    ldflags = ""
    // Caution : the order matters !
    libs = "../../src/c/libaccsum";

    includes_src_c = ilib_include_flag(get_absolute_file_path("builder_gateway_c.sce") + "../../src/c");

    tbx_build_gateway(libname, namelist, files, gateway_path, libs, "", includes_src_c);
endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
